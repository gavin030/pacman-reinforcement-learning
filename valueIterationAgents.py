# valueIterationAgents.py
# -----------------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import itertools
import numpy as np
import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
  """
      * Please read learningAgents.py before reading this.*

      A ValueIterationAgent takes a Markov decision process
      (see mdp.py) on initialization and runs value iteration
      for a given number of iterations using the supplied
      discount factor.
  """
  def __init__(self, mdp, discount = 0.9, iterations = 100):
    """
      Your value iteration agent should take an mdp on
      construction, run the indicated number of iterations
      and then act according to the resulting policy.
    
      Some useful mdp methods you will use:
          mdp.getStates()
          mdp.getPossibleActions(state)
          mdp.getTransitionStatesAndProbs(state, action)
          mdp.getReward(state, action, nextState)
    """
    self.mdp = mdp
    self.discount = discount
    self.iterations = iterations
    self.values = util.Counter() # A Counter is a dict with default 0
     
    "*** YOUR CODE HERE ***"
    states = self.mdp.getStates()
    self.values = {s : 0 for s in states}
    self.policy = {s : None for s in states}

    
    for i in range(self.iterations):
      Vi = dict(self.values)
      for s in states:
        if self.mdp.isTerminal(s): continue
        #if type(self.mdp.grid[s[0]][s[1]]) == int or type(self.mdp.grid[s[0]][s[1]]) == float: self.values[s] = self.mdp.grid[s[0]][s[1]]
        VMax = None
        actions = self.mdp.getPossibleActions(s)
        for a in actions:
          V = sum([p * (self.mdp.getReward(s, a, s_next) + self.discount * Vi[s_next]) for s_next, p in self.mdp.getTransitionStatesAndProbs(s, a)])
          if VMax == None or V > VMax:
            VMax = V
            self.policy[s] = a
        if type(self.mdp.grid[s[0]][s[1]]) == int or type(self.mdp.grid[s[0]][s[1]]) == float: self.policy[s] = 'exit'
        self.values[s] = VMax
    #for x, y in itertools.product(range(self.mdp.grid.width), range(self.mdp.grid.height)):
      #if type(self.mdp.grid[x][y]) == int: self.values[(x, y)] = self.mdp.grid[x][y]
        
    
    
  def getValue(self, state):
    """
      Return the value of the state (computed in __init__).
    """
    return self.values[state]


  def getQValue(self, state, action):
    """
      The q-value of the state action pair
      (after the indicated number of value iteration
      passes).  Note that value iteration does not
      necessarily create this quantity and you may have
      to derive it on the fly.
    """
    "*** YOUR CODE HERE ***"
    s = state
    a = action
    if self.mdp.isTerminal(s): return 0.0
    return sum([p * (self.mdp.getReward(s, a, s_next) + self.discount * self.values[s_next]) for s_next, p in self.mdp.getTransitionStatesAndProbs(s, a)])

  def getPolicy(self, state):
    """
      The policy is the best action in the given state
      according to the values computed by value iteration.
      You may break ties any way you see fit.  Note that if
      there are no legal actions, which is the case at the
      terminal state, you should return None.
    """
    "*** YOUR CODE HERE ***"
    return self.policy[state]

  def getAction(self, state):
    "Returns the policy at the state (no exploration)."
    return self.getPolicy(state)
  
